#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Ink Tesseract - a partially completed text recognition extension
# An Inkscape 1.1+ extension
##############################################################################

import shutil
import subprocess
import time
import sys, os
import uuid
from copy import deepcopy
import random

import inkex
from inkex import command, base, Color

from inklinea import Inklin
import ink_hocr

# Set file path for binary on Windows
# Should not be needed for Linux

if Inklin.os_check(None) == "windows":
    # inkex.errormsg('Windows Found !')
    tesseract_filepath = 'C:\\Program Files\\Tesseract-OCR\\tesseract.exe'
else:
    tesseract_filepath = 'tesseract'


def txt_to_text_elements(self, text_file, parent, mode='lines'):
    text_lines = text_file_to_line_list(self, text_file)
    text_list = text_lines_to_list(self, text_lines)
    # inkex.errormsg(text_list)
    text_element_list = Inklin.list_to_text_elements(self, parent, text_list)
    return text_element_list


def text_file_to_line_list(self, text_file):
    with open(text_file, 'r') as textfile:
        text_lines = textfile.read().splitlines()
        return text_lines


def text_lines_to_list(self, text_lines):
    text_list = []
    for line in text_lines:
        if line.strip() != '':
            text_list.append(line)
    return text_list


def run_tesseract(self, input_filepath, temp_folder_path):
    # Set export formats

    # Lets look to see which file format user wants to save:
    export_format_list = []

    if self.options.export_alto_cb == 'true':
        alto_out = r'alto'
        export_format_list.append('xml')
    else:
        alto_out = ''
    if self.options.export_hocr_cb == 'true':
        hocr_out = 'hocr'
        export_format_list.append(hocr_out)
    else:
        hocr_out = ''
    if self.options.export_pdf_cb == 'true':
        pdf_out = 'pdf'
        export_format_list.append(pdf_out)
    else:
        pdf_out = ''
    if self.options.export_tsv_cb == 'true':
        tsv_out = 'tsv'
        export_format_list.append(tsv_out)
    else:
        tsv_out = ''
    if self.options.export_txt_cb == 'true':
        txt_out = 'txt'
        export_format_list.append(txt_out)
    else:
        txt_out = ''
    if self.options.export_get_images_cb == 'true':
        images_out = 'get.images'
        # export_format_list.append(images_out)
    else:
        images_out = ''
    if self.options.export_logfile_cb == 'true':
        logfile_out = 'logfile'
        # export_format_list.append(logfile_out)
    else:
        logfile_out = ''
    if self.options.export_lstm_train_cb == 'true':
        lstm_train_out = 'lstm.train'
    else:
        lstm_train_out = ''
    if self.options.export_makebox_cb == 'true':
        makebox_out = 'makebox'
        export_format_list.append('box')
    else:
        makebox_out = ''

    # set --psm
    psm = self.options.psm_combo
    psm_option = '--psm'
    psm_mode_no = str(psm)

    # set --oem
    oem = self.options.oem_combo
    oem_option = '--oem'
    oem_mode_no = str(oem)

    # set --dpi
    if self.options.custom_dpi_cb == 'true':
        dpi_option = '--dpi'
        dpi_int = str(self.options.dpi_int)
        # inkex.errormsg(f'{dpi_option} {dpi_int}')
    else:
        dpi_option = ''
        dpi_int = ''

    # Create a unique temp filename based on epoch time for tesseract to dump it's files to.
    # tess_temp_filename = (str(time.time())).replace('.', '')
    tess_temp_filename = str(uuid.uuid1())

    tess_temp_filename_path = Inklin.check_output_filepath(self, temp_folder_path, tess_temp_filename)

    # There is a depreciation message triggered by the command module for higher versions of python
    # Let's temporarily turn off stdout and stderr
    Inklin.set_stdout(self, 'off')
    Inklin.set_stderr(self, 'off')

    command.call(tesseract_filepath, input_filepath, tess_temp_filename_path, oem_option, oem_mode_no, psm_option,
                 psm_mode_no, dpi_option, dpi_int, alto_out, hocr_out, pdf_out, tsv_out, txt_out, images_out,
                 logfile_out, makebox_out)

    # Let's turn stdout and stderr back on
    Inklin.set_stderr(self, 'on')
    Inklin.set_stdout(self, 'on')

    # If file output is required, lets copy those files from the temp folder
    # Into the user export folder
    if self.options.file_output_cb == 'true':

        # Set Export base filename and also
        base_filename = self.options.base_filename_string
        export_folder_path = self.options.export_folder
        base_filename_path = Inklin.check_output_filepath(self, export_folder_path, base_filename)
        temp_file_list = os.listdir(temp_folder_path)

        if self.options.subfolder_output_cb == 'true':
            folder_name_format = self.options.subfolder_format_combo
            if folder_name_format == 'timestamp':
                subfolder_name = str(time.time()).replace('.', '')
            if folder_name_format == 'uuid':
                subfolder_name = str(uuid.uuid4())
            if folder_name_format == 'random':
                subfolder_name = str(random.randrange(1000000, 9999999))

            subfolder_path = os.path.join(export_folder_path, subfolder_name)
            os.mkdir(subfolder_path)
            export_folder_path = subfolder_path

        for filename in temp_file_list:
            if filename.split('.')[-1] in export_format_list:
                new_filename = str(base_filename) + '_' + 'psm_' + str(psm_mode_no) + '_oem_' + str(
                    oem_mode_no) + '_' + str(filename)
                filepath = os.path.join(temp_folder_path, filename)
                new_filepath = os.path.join(export_folder_path, new_filename)
                shutil.copy2(filepath, new_filepath)


def get_hocr_offset(self, svg_file, boundary_object_id):
    bbox_dict = Inklin.inkscape_command_call_bboxes_to_dict(self, svg_file)
    for item in bbox_dict.items():
        if boundary_object_id in item[0]:
            offset_x = bbox_dict[item[0]]['x1']
            offset_y = bbox_dict[item[0]]['y1']
            # inkex.errormsg(f'X: {offset_x}  Y{offset_y}')
            return offset_x, offset_y


def layout_from_hocr(self, temp_folder_path, sampling_dpi_cf, color_dict, hocr_offset=0):
    hocr_etree = ink_hocr.newest_hocr_file_to_etree(self, temp_folder_path)

    # inkex.errormsg(hocr_etree)
    if self.options.canvas_output_cb == 'true':
        if self.options.line_paths_cb == 'false':
            line_paths_display = 'none'
        else:
            line_paths_display = 'show'
        if self.options.word_paths_cb == 'false':
            word_paths_display = 'none'
        else:
            word_paths_display = 'show'

        if hocr_etree == None:
            inkex.errormsg('No Files Found')

        else:
            hocr_layer = Inklin.create_new_group(self, self.svg, 'hocr', 'layer', 'epoch')
            if self.options.word_text_cb == 'true' or self.options.word_paths_cb == 'true':
                ocr_element_class = 'ocrx_word'
                element_list = ink_hocr.get_element_list(self, ocr_element_class, hocr_etree)
                element_dict = ink_hocr.make_element_dict(self, element_list)
                if self.options.word_text_cb == 'true':
                    text_display = 'show'
                    ink_hocr.element_dict_to_text_boxes(self, hocr_layer, hocr_offset, element_dict, sampling_dpi_cf, color_dict['word_text_cp'], text_display)
                if self.options.word_paths_cb == 'true':
                    paths_display = 'show'
                    ink_hocr.element_dict_to_box_paths(self, hocr_layer, hocr_offset, element_dict, sampling_dpi_cf, color_dict['word_paths_cp'], paths_display)

            # If ocr_line is called, lets save the ocr_line element list if ocr_par is required
            # This saves repeating the operation.

            if self.options.line_text_cb == 'true' or self.options.line_paths_cb == 'true':
                ocr_element_class = 'ocr_line'
                element_list = ink_hocr.get_element_list(self, ocr_element_class, hocr_etree)
                element_dict = ink_hocr.make_element_dict(self, element_list)
                if self.options.paragraph_text_cb == 'true' or self.options.paragraph_paths_cb == 'true':
                    ocr_line_dict = deepcopy(element_dict)
                if self.options.line_text_cb == 'true':
                    text_display = 'show'
                    ink_hocr.element_dict_to_text_boxes(self, hocr_layer, hocr_offset, element_dict, sampling_dpi_cf, color_dict['line_text_cp'], text_display)
                if self.options.line_paths_cb == 'true':
                    paths_display = 'show'
                    ink_hocr.element_dict_to_box_paths(self, hocr_layer, hocr_offset, element_dict, sampling_dpi_cf, color_dict['line_paths_cp'], paths_display)

            if self.options.paragraph_text_cb == 'true' or self.options.paragraph_paths_cb == 'true':
                if 'ocr_line_dict' in locals():
                    None
                else:
                    element_list = ink_hocr.get_element_list(self, 'ocr_line', hocr_etree)
                    ocr_line_dict = ink_hocr.make_element_dict(self, element_list)

                ocr_element_class = 'ocr_par'
                element_list = ink_hocr.get_element_list(self, ocr_element_class, hocr_etree)
                element_dict = ink_hocr.make_element_dict(self, element_list)
                element_list = ink_hocr.get_element_list(self, ocr_element_class, hocr_etree)

                if self.options.paragraph_text_cb == 'true':
                    text_display = 'show'
                    ink_hocr.element_dict_to_flow_text_boxes(self, element_list, hocr_etree, hocr_layer, hocr_offset, element_dict, ocr_line_dict, sampling_dpi_cf, color_dict['par_text_cp'], text_display)
                if self.options.paragraph_paths_cb == 'true':
                    paths_display = 'show'
                    ink_hocr.element_dict_to_box_paths(self, hocr_layer, hocr_offset, element_dict, sampling_dpi_cf, color_dict['par_paths_cp'], paths_display)

            if self.options.page_text_cb == 'true' or self.options.page_paths_cb == 'true':
                if 'ocr_line_dict' in locals():
                    None
                else:
                    element_list = ink_hocr.get_element_list(self, 'ocr_line', hocr_etree)
                    ocr_line_dict = ink_hocr.make_element_dict(self, element_list)

                ocr_element_class = 'ocr_page'
                element_list = ink_hocr.get_element_list(self, ocr_element_class, hocr_etree)
                element_dict = ink_hocr.make_element_dict(self, element_list)
                element_list = ink_hocr.get_element_list(self, ocr_element_class, hocr_etree)

                if self.options.page_text_cb == 'true':
                    text_display = 'show'
                    ink_hocr.element_dict_to_flow_text_boxes(self, element_list, hocr_etree, hocr_layer, hocr_offset, element_dict, ocr_line_dict, sampling_dpi_cf, color_dict['page_text_cp'], text_display)
                if self.options.page_paths_cb == 'true':
                    paths_display = 'show'
                    ink_hocr.element_dict_to_box_paths(self, hocr_layer, hocr_offset, element_dict, sampling_dpi_cf, color_dict['page_paths_cp'], paths_display)

            # Content area container
            if self.options.content_area_text_cb == 'true' or self.options.content_area_paths_cb == 'true':
                if 'ocr_line_dict' in locals():
                    None
                else:
                    element_list = ink_hocr.get_element_list(self, 'ocr_line', hocr_etree)
                    ocr_line_dict = ink_hocr.make_element_dict(self, element_list)

            ocr_element_class = 'ocr_carea'
            element_list = ink_hocr.get_element_list(self, ocr_element_class, hocr_etree)
            element_dict = ink_hocr.make_element_dict(self, element_list)
            element_list = ink_hocr.get_element_list(self, ocr_element_class, hocr_etree)

            if self.options.content_area_text_cb == 'true':
                text_display = 'show'
                ink_hocr.element_dict_to_flow_text_boxes(self, element_list, hocr_etree, hocr_layer, hocr_offset, element_dict, ocr_line_dict, sampling_dpi_cf, color_dict['carea_text_cp'], text_display)
            if self.options.content_area_paths_cb == 'true':
                paths_display = 'show'
                ink_hocr.element_dict_to_box_paths(self, hocr_layer, hocr_offset, element_dict, sampling_dpi_cf, color_dict['carea_paths_cp'], paths_display)

    # Raw Text Output
    if self.options.raw_text_cb == 'true':
        raw_text = ink_hocr.get_raw_text(self, hocr_etree)
        inkex.errormsg(raw_text)

class InkTesseract(inkex.EffectExtension):

    def add_arguments(self, pars):
        # Notebook
        pars.add_argument("--ink_tesseract_notebook", type=str, dest="ink_tesseract_notebook", default=0)

        # Image sampling dpi
        pars.add_argument("--sampling_dpi_cb", type=str, dest="sampling_dpi_cb")
        pars.add_argument("--sampling_dpi_int", type=int, dest="sampling_dpi_int", default=300)

        # Svg custom dpi
        pars.add_argument("--svg_dpi_cb", type=str, dest="svg_dpi_cb")
        pars.add_argument("--svg_dpi_int", type=int, dest="svg_dpi_int", default=96)

        # Image Source
        pars.add_argument("--image_source_combo", type=str, dest="image_source_combo", default='selected')

        # Input File
        pars.add_argument("--external_image_filepath", type=str, dest="external_image_filepath", default=None)

        # Text Elements
        pars.add_argument("--text_elements_combo", type=str, dest="text_elements_combo", default='0')

        #  Output Options
        pars.add_argument("--canvas_output_cb", type=str, dest="canvas_output_cb")
        pars.add_argument("--file_output_cb", type=str, dest="file_output_cb")
        pars.add_argument("--raw_text_cb", type=str, dest="raw_text_cb")
        pars.add_argument("--export_folder", type=str, dest="export_folder", default=None)

        # Base Export Filename
        pars.add_argument("--base_filename_string", type=str, dest="base_filename_string", default='x')

        # Subfolders
        pars.add_argument("--subfolder_output_cb", type=str, dest="subfolder_output_cb")
        pars.add_argument("--subfolder_format_combo", type=str, dest="subfolder_format_combo", default='selected')

        # Canvas
        pars.add_argument("--page_text_cb", type=str, dest="page_text_cb")
        pars.add_argument("--page_text_cp", type=str, dest="page_text_cp")
        pars.add_argument("--page_paths_cb", type=str, dest="page_paths_cb")
        pars.add_argument("--page_paths_cp", type=str, dest="page_paths_cp")

        pars.add_argument("--content_area_text_cb", type=str, dest="content_area_text_cb")
        pars.add_argument("--content_area_text_cp", type=str, dest="content_area_text_cp")
        pars.add_argument("--content_area_paths_cb", type=str, dest="content_area_paths_cb")
        pars.add_argument("--content_area_paths_cp", type=str, dest="content_area_paths_cp")

        pars.add_argument("--paragraph_text_cb", type=str, dest="paragraph_text_cb")
        pars.add_argument("--paragraph_text_cp", type=str, dest="paragraph_text_cp")
        pars.add_argument("--paragraph_paths_cb", type=str, dest="paragraph_paths_cb")
        pars.add_argument("--paragraph_paths_cp", type=str, dest="paragraph_paths_cp")

        pars.add_argument("--line_text_cb", type=str, dest="line_text_cb")
        pars.add_argument("--line_text_cp", type=str, dest="line_text_cp")
        pars.add_argument("--line_paths_cb", type=str, dest="line_paths_cb")
        pars.add_argument("--line_paths_cp", type=str, dest="line_paths_cp")

        pars.add_argument("--word_text_cb", type=str, dest="word_text_cb")
        pars.add_argument("--word_text_cp", type=str, dest="word_text_cp")
        pars.add_argument("--word_paths_cb", type=str, dest="word_paths_cb")
        pars.add_argument("--word_paths_cp", type=str, dest="word_paths_cp")

        # PSM Combo
        pars.add_argument("--psm_combo", type=str, dest="psm_combo", default='0')

        # Oem Combo
        pars.add_argument("--oem_combo", type=str, dest="oem_combo", default='0')

        # Export configurations
        pars.add_argument("--export_alto_cb", type=str, dest="export_alto_cb")
        pars.add_argument("--export_hocr_cb", type=str, dest="export_hocr_cb")
        pars.add_argument("--export_pdf_cb", type=str, dest="export_pdf_cb")
        pars.add_argument("--export_tsv_cb", type=str, dest="export_tsv_cb")
        pars.add_argument("--export_txt_cb", type=str, dest="export_txt_cb")
        pars.add_argument("--export_get_images_cb", type=str, dest="export_get_images_cb")
        pars.add_argument("--export_logfile_cb", type=str, dest="export_logfile_cb")
        pars.add_argument("--export_lstm_train_cb", type=str, dest="export_lstm_train_cb")
        pars.add_argument("--export_makebox_cb", type=str, dest="export_makebox_cb")

        # Dpi settings
        pars.add_argument("--custom_dpi_cb", type=str, dest="custom_dpi_cb")
        pars.add_argument("--dpi_int", type=int, dest="dpi_int", default=300)

    def effect(self):

        # Text and path colours
        color_dict = {}
        color_dict['page_text_cp'] = Inklin.rgb_long_to_svg_rgb(self, self.options.page_text_cp)
        color_dict['page_paths_cp'] = Inklin.rgb_long_to_svg_rgb(self, self.options.page_paths_cp)
        color_dict['carea_text_cp'] = Inklin.rgb_long_to_svg_rgb(self, self.options.content_area_text_cp)
        color_dict['carea_paths_cp'] = Inklin.rgb_long_to_svg_rgb(self, self.options.content_area_paths_cp)
        color_dict['par_text_cp'] = Inklin.rgb_long_to_svg_rgb(self, self.options.paragraph_text_cp)
        color_dict['par_paths_cp'] = Inklin.rgb_long_to_svg_rgb(self, self.options.paragraph_paths_cp)
        color_dict['line_text_cp'] = Inklin.rgb_long_to_svg_rgb(self, self.options.line_text_cp)
        color_dict['line_paths_cp'] = Inklin.rgb_long_to_svg_rgb(self, self.options.line_paths_cp)
        color_dict['word_text_cp'] = Inklin.rgb_long_to_svg_rgb(self, self.options.word_text_cp)
        color_dict['word_paths_cp'] = Inklin.rgb_long_to_svg_rgb(self, self.options.word_paths_cp)

        found_units = self.svg.unit
        self.cf = Inklin.conversions[found_units]

        # User Svg dpi
        if self.options.svg_dpi_cb == 'true':
            svg_dpi = self.options.svg_dpi_int
        else:
            svg_dpi = 96

        # Conversion factor for user sampling dpi
        if self.options.sampling_dpi_cb == 'true':
            sampling_dpi = self.options.sampling_dpi_int
            self.sampling_dpi_cf = sampling_dpi_cf = (svg_dpi / sampling_dpi)
        else:
            sampling_dpi = svg_dpi
            self.sampling_dpi_cf = 1

        hocr_offset = [0, 0]

        # Lets create a temp folder for tesseract file output
        temp_folder_path = Inklin.make_temp_folder(self)

        # Choose source which can be a selection list
        # Or an external file
        source_type = self.options.image_source_combo

        # selection = self.svg.selected[0]
        selection_list = self.svg.selected

        if source_type == 'selected':
            if len(selection_list) < 1:
                inkex.errormsg('Nothing Selected')
                sys.exit()

            # temp_svg = deepcopy(self.svg)
            png_filepath = Inklin.inkscape_command_call_png(self, self.options.input_file, temp_folder_path,
                                                            sampling_dpi, 'export-area-drawing')
            hocr_offset = get_hocr_offset(self, self.options.input_file, 'svg')
            input_filepath = png_filepath

        if source_type == 'page':
            png_filepath = Inklin.inkscape_command_call_png(self, self.options.input_file, temp_folder_path,
                                                            sampling_dpi, 'export-area-page')
            input_filepath = png_filepath

        if source_type == 'drawing':
            png_filepath = Inklin.inkscape_command_call_png(self, self.options.input_file, temp_folder_path,
                                                            sampling_dpi, 'export-area-drawing')
            hocr_offset = get_hocr_offset(self, self.options.input_file, 'svg')
            input_filepath = png_filepath

        if source_type == 'file':
            input_filepath = self.options.external_image_filepath
            # If the file is an svg, lets process it into bitmap
            extension = input_filepath.split('.')[-1]
            if extension.lower() == 'svg':
                with open(input_filepath, 'r') as svgfile:
                    svgtext = svgfile.read()
                    input_filepath = Inklin.svg_to_pixbuf_to_pngfile(self, svgtext, temp_folder_path)

        run_tesseract(self, input_filepath, temp_folder_path)

        layout_from_hocr(self, temp_folder_path, self.sampling_dpi_cf, color_dict, hocr_offset)

        # Cleanup temp folder
        if hasattr(self, 'inklin_temp_folder'):
            shutil.rmtree(self.inklin_temp_folder)


if __name__ == '__main__':
    InkTesseract().run()
