#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Inlinea library - Some reusable functions I wrote for other extensions
# An Inkscape 1.1+
##############################################################################


import uuid

import inkex

# Only import standard libraries here
# Otherwise if a system with
import random
import time


class Inklin:
    # Unit conversions
    conversions = {
        'in': 96.0,
        'pt': 1.3333333333333333,
        'px': 1.0,
        'mm': 3.779527559055118,
        'cm': 37.79527559055118,
        'm': 3779.527559055118,
        'km': 3779527.559055118,
        'Q': 0.94488188976378,
        'pc': 16.0,
        'yd': 3456.0,
        'ft': 1152.0,
        '': 1.0,  # Default px
    }

    # Workaround for Inkscape 1.1+ shape/image units bug.
    def shape_unit_fix(self):
        """
        Inkscape 1.1 returns paths in user units but shapes and images in pixels. \n
        This is a user selectable conversions factor ( usually a checkbox ) \n
        which is stored at self.conversion_factor
        """
        found_units = self.svg.unit
        if self.options.shape_unit_fix == 'true':
            self.conversion_factor = Inklin.conversions[found_units]
        else:
            self.conversion_factor = 1

    # Could not find simplestyle, found this instead in extensions repo
    def formatStyle(a):
        """Format an inline style attribute from a dictionary"""
        return ";".join([att + ":" + str(val) for att, val in a.items()])

    # Platform Check
    ################
    def os_check(self):
        """
        Check which OS we are using
        :return: OS Name ( windows, linux, macos )
        """
        from sys import platform

        if 'linux' in platform.lower():
            return 'linux'
        elif 'darwin' in platform.lower():
            return 'macos'
        elif 'win' in platform.lower():
            return 'windows'

    # Functions to silence stderr and stdout
    # Close output pipeline ( See notes at top of script )
    def set_stdout(self, state):
        import sys, os
        if state == 'off':
            sys.stdout = open(os.devnull, 'w')
        else:
            sys.stdout.close()
            sys.stdout = sys.__stdout__

    def set_stderr(self, state):
        import sys, os
        if state == 'off':
            sys.stderr = open(os.devnull, 'w')
        else:
            sys.stderr.close()
            sys.stderr = sys.__stderr__

    # Function to list object attributes
    ####################################

    def get_attributes(self):
        """ Returns a string containing all object attributes
             - One attribute per line
        """
        attribute_string = 'test'
        for att in dir(self):
            try:
                attribute = (att, getattr(self, att))
                attribute_string = attribute_string + str(attribute) + '\n'
            except:
                None
        return attribute_string

    # Extra path functions
    #######################

    # Return a rectangle path

    # Create Groups
    def create_new_group(self, parent, prefix, mode, suffix_type='epoch'):
        """
        Create a new group or Inkscape layer in document and return group

        :param parent: Parent group will be appended to
        :param prefix: The prefix for the group name
        :param mode: Inkscape 'layer' or 'group'
        :param suffix_type: The suffix for the group name
        :return: Group Object
        """

        from inkex import Group

        if suffix_type == 'random':
            id_suffix = str(random.randrange(1000000, 9999999))
        if suffix_type == 'epoch':
            id_suffix = str(time.time())
        if suffix_type == 'none':
            id_suffix = 'layer'

        group_id = str(prefix) + '_' + id_suffix
        new_group = parent.add(Group.new(group_id))
        new_group.set('inkscape:groupmode', str(mode))
        new_group.attrib['id'] = group_id

        return new_group

    def list_to_text_elements(self, parent, text_list):
        from inkex import TextElement, Tspan
        id_prefix = 'text' + str(random.randrange(1, 9999))
        index = 0
        text_element_list = []
        for item in text_list:
            text_element = TextElement()
            text_element_id = id_prefix + '_' + str(index)
            text_element.set('id', text_element_id)
            tspan_element = Tspan()
            tspan_element.text = item
            text_element.append(tspan_element)
            parent.append(text_element)
            text_element_list.append(text_element)
            index += 1
        # inkex.errormsg(text_element_list)
        return text_element_list

    def extract_selected_object_svg(self, source_svg, selected_object):
        from copy import deepcopy
        # Let's keep looping until we run out of <g> elements
        current_level = selected_object
        parent_level = selected_object
        level_list = []
        level_id_list = []

        while parent_level != None:
            current_level = parent_level
            parent_level = current_level.getparent()

            current_level_item = current_level
            level_list.append(current_level_item)

        # Reverse the list, now we have <svg>-<g>-<g>-object order
        level_list.reverse()

        # Lets get the svg shell

        shell_svg = Inklin.get_inkscape_svg_shell(self, source_svg)

        # Lets ignore the <svg> and the original object

        chain_list = []
        chain_list_index = 0
        if len(level_list[1:-1]) < 0:
            for old_group in level_list[1:-1]:
                new_group = inkex.Group()
                # inkex.errormsg(new_group)
                new_group.style = old_group.style
                new_group.transform = old_group.transform

                # Then start building the chain
                # level_list[0] is the svg
                chain_list.append(new_group)
                if chain_list_index > 0:
                    new_group.append(chain_list[chain_list_index - 1])
                    chain_list_index += 1
                else:
                    shell_svg.append(new_group)
        else:
            new_group = shell_svg
        # Duplicate the original object and add to chain
        final_object = selected_object.duplicate()

        new_group.append(final_object)

        return shell_svg

    # Colour functions
    ##################

    def random_rgb_string(self):
        """
        Return a random rgb colour string \n
        rgb(0-255, 0-255, 0-255)
        """
        random_red = random.randrange(0, 255)
        random_green = random.randrange(0, 255)
        random_blue = random.randrange(0, 255)

        return f'rgb({random_red}, {random_green}, {random_blue})'

    def rgb_long_to_svg_rgb(self, rgb_long):
        from inkex import Color
        my_rgb = Color.parse_str(rgb_long)
        my_rgb_numbers = str(my_rgb[1]).replace('[', '').replace(']', '')
        svg_rgb = f'rgb({my_rgb_numbers})'
        return svg_rgb

    # Could not find simplestyle, found this instead in extensions repo
    def format_style(my_style):
        """Format an inline style attribute from a dictionary"""
        return ";".join([att + ":" + str(val) for att, val in my_style.items()])

    # Image functions
    #################

    def embedded_image_to_PIL(self, embedded_image):
        """
        If <image> is embedded, do base64string to PIL image
        """

        from PIL import Image
        from io import BytesIO
        import base64

        my_href = embedded_image.get('xlink:href')
        base64_string = my_href.split('base64,')[1]
        img_stream = BytesIO()
        img_stream.write(base64.b64decode(base64_string))
        im = Image.open(img_stream)
        return im

    def linked_image_to_pil(self, image_path):
        """
        If <image> is linked, open and return PIL image.
        """
        from PIL import Image
        im = Image.open(image_path)
        # inkex.errormsg((im.format, im.size, im.mode))
        im.close()
        return im

    def image_to_pil(self, image_element):
        """
        Return a PIL image from an <image> ( linked or embedded )
        """
        if image_element.TAG.lower() == 'image':
            # base64 in string indicated embedded image
            if 'base64' in image_element.tostring().decode('utf-8').lower():
                im = Inklin.embedded_image_to_PIL(self, image_element)
                return im

            # Otherwise it will be a linked image
            else:
                try:
                    image_path = image_element.get('xlink:href')
                    image_real_path = image_path.split('file://')[1]
                    im = Inklin.linked_image_to_pil(self, image_real_path)
                    return im
                except:
                    return None

    def image_to_png(self, image_element):
        import os
        pil_image = Inklin.image_to_pil(self, image_element)
        temp_folder = Inklin.make_temp_folder(self)
        temp_png_filename = str(uuid.uuid1()) + '.png'
        temp_png_filepath = os.path.join(temp_folder, temp_png_filename)
        temp_png = pil_image.save(temp_png_filepath, 'png')
        return temp_png_filepath

    def pil_to_base64_image_string(self, im):
        """
        Return a png base64 string from a PIL image
        :param im: PIL image
        :return: Base64 encoded png image
        """
        from PIL import Image
        from io import BytesIO
        import base64

        img_stream = BytesIO()
        im.save(img_stream, format='PNG')
        byte_img = img_stream.getvalue()
        base64_img_str = base64.b64encode(byte_img).decode('utf-8')

        return base64_img_str

    def pil_to_base64_image_object(self, parent, base_id, im):
        """
        Create an <image> object with base64 string (embedded) png image on canvas \n
        :param base_id: The id of the created object
        :param im: The PIL source image
        """
        from lxml import etree
        # Inscape 1.1
        conversion_factor = self.conversion_factor

        my_image = etree.SubElement(self.svg, inkex.addNS('image', 'svg'))

        conversion_factor = self.conversion_factor

        base64_img_str = Inklin.pil_to_base64_image_string(self, im)

        # Lets place the preview image off the canvas
        my_image.attrib['x'] = str(0)
        my_image.attrib['y'] = str((-(im.size[1]) / conversion_factor))

        my_image.attrib['width'] = str(im.size[0] / conversion_factor)
        my_image.attrib['height'] = str(im.size[1] / conversion_factor)

        my_image.attrib['id'] = f'{base_id}'

        my_image.set('xlink:href', str(f'data:image/png;base64,{base64_img_str}'))

    def svg_to_pixbuf_to_pngfile(self, svg, temp_folder):
        Inklin.set_stderr(self, 'off')
        import os
        import gi
        gi.require_version('Gtk', '3.0')
        from gi.repository import Gtk, GdkPixbuf, GObject
        loader = GdkPixbuf.PixbufLoader()
        loader.write(svg.encode())
        # loader.write(svg)
        loader.close()
        pixbuf = loader.get_pixbuf()

        # image_height = 10000
        #
        # height = pixbuf.props.height
        # width = pixbuf.props.width
        # # Determines the height of the preview image
        # scale_ratio = width / image_height
        # scaled_height = int(height / scale_ratio)
        #
        # pixbuf = pixbuf.scale_simple(image_height, scaled_height, InterpType.BILINEAR)

        pixbuf_png_filepath = os.path.join(temp_folder, 'pixbuf.png')
        pixbuf.savev(pixbuf_png_filepath, 'png')
        Inklin.set_stderr(self, 'on')
        return pixbuf_png_filepath

    def inkscape_rasterise(self, vector_source, source_svg, png_folder, export_area='export-area-drawing'):
        import os
        """
        Takes input from a vector source and outputs a bitmap image (.png)
        :param vector_source: Vector source - element list ( single item should still be a list of 1 )
        :param rasterise_type: Should we rasterise a selection or page or the entire drawing ?
        :return: png_filepath
        """
        if len(vector_source) < 1:
            inkex.errormsg('No selection found')
            return
        else:
            selection_id_list = []
            for element in vector_source:
                selection_id_list.append(element.get_id())
            selection_string = ','.join(selection_id_list)

            png_filepath = os.path.join(png_folder, 'inkscape_selection.png')

            # Check for Inkscape 1.2+
            # Verbs became actions in 1.2+ different command line required.
            if int((inkex.__version__).split('.')[1]) > 1:

                option_list = ''

                action_list = f'select-by-id:{selection_string};' \
                              f'select-invert:no-layers;' \
                              f'delete-selection;' \
                              f'export-filename:{png_filepath};' \
                              f'{export_area};' \
                              f'export-do;'
            else:  # Inkscape 1.1
                option_list = '--batch-process'

                action_list = f'select-by-id:{selection_string};' \
                              f'select-invert:no-layers;' \
                              f'EditDelete;' \
                              f'export-filename:{png_filepath};' \
                              f'{export_area};' \
                              f'export-do;'

            # inkex.errormsg(selection_string)

            rasterise_temp_svg = Inklin.make_temp_file_from_svg_object(self, source_svg)

            rasterise_temp_svg.close()
            Inklin.set_stderr(self, 'off')
            command_temp_svg = Inklin.inkscape_command_call(self, rasterise_temp_svg.name, option_list, action_list)
            Inklin.set_stderr(self, 'on')
            command_temp_svg.close()
            # inkex.errormsg(rasterise_temp_svg)

            return png_filepath

    # Command Line
    ##############

    # Return a svg file copy in a temp folder
    # This is due to permission problems with tempfile in Windows

    def make_temp_folder(self):
        """
        Creates a temp folder to which files can be written \n
        To remove folder at end of script use: \n
        # Cleanup temp folder \n
        if hasattr(self, 'inklin_temp_folder'):
            shutil.rmtree(self.inklin_temp_folder)

        :return: A temp folder path string
        """
        import tempfile
        temp_folder = tempfile.mkdtemp()
        self.inklin_temp_folder = str(temp_folder)
        return temp_folder

    def make_temp_file_copy(self, my_file, extension='.svg'):
        import sys
        import shutil
        import os
        import uuid
        # Create random uuid - then add extension
        temp_file_name = str(uuid.uuid4()) + str(extension)
        if hasattr(self, 'inklin_temp_folder'):
            temp_folder = self.inklin_temp_folder
        else:
            temp_folder = Inklin.make_temp_folder(self)

        temp_file = shutil.copy(my_file, os.path.join(temp_folder, temp_file_name))
        temp_file_object = open(temp_file)
        temp_file_object.temp_folder = temp_folder
        return temp_file_object

    def make_temp_file_from_svg_object(self, svg_object):
        import os
        import time
        temp_file_name = str(uuid.uuid4()) + '.svg'
        # temp_file_name = str(time.time()).replace('.', '') + '.svg'
        svg_text = svg_object.tostring().decode('utf-8')
        if hasattr(self, 'inklin_temp_folder'):
            temp_folder = self.inklin_temp_folder
        else:
            temp_folder = Inklin.make_temp_folder(self)
        temp_filepath = os.path.join(temp_folder, temp_file_name)
        new_file = open(temp_filepath, 'w')
        new_file.write(svg_text)

        return new_file

    def inkscape_command_call(self, input_file, options_list, action_list):
        """
        A function to execute an Inkscape command call on a temp_file copy of the
        input_file and return the resulting file object. \n
       :param input_file: input file path ( usually self.options.input_file )
       :param options_list: inkscape command line options
       :param action_list: inkscape command line actions ( and verbs in Inskcape 1.1 )
       :return: Returns the resulting svg file object ( which must be .closed() later )
       """
        from inkex import command
        # First make a copy of the input_file
        temp_svg = Inklin.make_temp_file_copy(self, input_file)
        # Then run the command line
        command.inkscape(temp_svg.name, options_list, f'--actions={action_list}')
        # inkex.errormsg(temp_svg)
        return temp_svg

    def inkscape_command_call(self, input_file, option_list, action_list):
        """
        A function to execute an Inkscape command call on a temp_file copy of the
        input_file and return the resulting file object. \n
       :param input_file: input file path ( usually self.options.input_file )
       :param options_list: inkscape command line options
       :param action_list: inkscape command line actions ( and verbs in Inskcape 1.1 )
       :return: Returns the resulting svg file object ( which must be .closed() later )
       """
        from inkex import command
        # First make a copy of the input_file
        temp_svg = Inklin.make_temp_file_copy(self, input_file)
        # Then run the command line
        command.inkscape(temp_svg.name, option_list, f'--actions={action_list}')
        return temp_svg

    def inkscape_command_call_png(self, input_file, temp_folder_path, dpi=96, export_area="export-area-page", inkscape_version='1'):
        """
        A function to execute an Inkscape command call on a temp_file copy of the
        input_file and return the resulting file object. \n
       :param input_file: input file path ( usually self.options.input_file )
       :param options_list: inkscape command line options
       :param action_list: inkscape command line actions ( and verbs in Inskcape 1.1 )
       :return: Returns the resulting svg file object ( which must be .closed() later )
       """
        from inkex import command
        import os, sys

        png_filepath = os.path.join(temp_folder_path, f'inkscape_exported.png')

        action_list = f'export-filename:{png_filepath};' \
                      f'export-dpi:{dpi};' \
                      f'{export_area};' \
                      f'export-do;'

        # First make a copy of the input_file
        temp_svg = Inklin.make_temp_file_copy(self, input_file)
        # Then run the command line
        command.inkscape(temp_svg.name, f'--actions={action_list}')
        temp_svg.close()
        return png_filepath

    def inkscape_command_call_stdout(self, input_file, options_list, action_list):
        """
        A function to execute an Inkscape command call on a temp_file copy of the
        input_file and return stdout only. Useful for select-list etc.\n
        :param input_file: input file path ( usually self.options.input_file )
        :param options_list: inkscape command line options
        :param action_list: inkscape command line actions ( and verbs in Inskcape 1.1 )
        :return: Returns the stdout from the command line call
        """
        from inkex import command
        # First make a copy of the input_file
        temp_svg = Inklin.make_temp_file_copy(self, input_file)
        # Then run the command line
        stdout = command.inkscape(temp_svg.name, options_list, f'--actions={action_list}')
        temp_svg.close()
        return stdout

    def inkscape_command_call_bboxes_to_dict(self, input_file):
        """
        A function to return a dictionary of all element bounding boxes
        -- this function is visual ( includes stroke etc ) rather than just
        geometric ( just path bbox ) \n
        :param input_file: input file path ( usually self.options.input_file )
        :return: Returns the results of --query-all in a dictionary
        """
        from inkex import command
        # First make a copy of the input_file
        temp_svg = Inklin.make_temp_file_copy(self, input_file)
        # Then run the command line
        my_query = command.inkscape(temp_svg.name, '--query-all')

        # Account for versions of inkey.py which return query as bytes
        if type(my_query) != str:
            my_query = my_query.decode("utf-8")
        # --query-all produces multiline output of the following format
        # path853,172.491,468.905,192.11,166.525 - as string
        # ElementId, Top, Left, Width, Height

        # Make a list splitting by each new line
        my_query_items = my_query.split('\n')
        my_element_bbox_dict = {}

        for my_query_item in my_query_items:
            # Create a comma separated list item for each line
            my_element = my_query_item.split(',')
            # Make a dictionary for all elements, rejected malformed elements.
            if len(my_element) > 4:
                my_element_bbox_dict[my_element[0]] = {}
                # Create Dictionary entry in anticlockwise format
                # x1 = TopLeft, x2 = BottomLeft, x3 = BottomRight, x4 = TopRight, mid_x and mid_y

                # First convert all values to float, skipping element id ( first entry )
                my_element_bbox = [float(x) for x in my_element[1:]]

                width = my_element_bbox[2]
                height = my_element_bbox[3]

                x1 = my_element_bbox[0]
                y1 = my_element_bbox[1]
                x2 = x1
                y2 = y1 + height
                x3 = x1 + width
                y3 = y2
                x4 = x1 + width
                y4 = y1
                mid_x = x1 + width / 2
                mid_y = y1 + height / 2

                my_element_bbox_dict[my_element[0]].update(x1=x1, y1=y1, x2=x2, y2=y2, x3=x3, y3=y3, x4=x4, y4=y4,
                                                           mid_x=mid_x, mid_y=mid_y, width=width, height=height)
        # Return dictionary
        temp_svg.close()
        return my_element_bbox_dict

    # Check folder is writeable

    def check_filepath_writable(self, folderpath, filename):
        import os

        filepath = os.path.join(folderpath, filename)

        try:
            try_file = open(filepath, 'w')
            try_file.close()
            os.remove(filepath)
            return True
        except:
            # inkex.errormsg('filepath is not valid ( or non writeable ) ')
            return False

    def check_output_filepath(self, export_folder, base_filename):
        import os, sys
        # Test Export Folder writable with known good filename
        test_filename = (str(time.time())).replace('.', '')
        if Inklin.check_filepath_writable(self, export_folder, test_filename):
            # inkex.errormsg('Export Folder is Writeable')
            None
        else:
            inkex.errormsg('Export Folder is *Not Writeable*')
            sys.exit()
        # Test Export Filename
        if Inklin.check_filepath_writable(self, export_folder, base_filename):
            # inkex.errormsg('Export Folder is okay')
            export_filepath = os.path.join(export_folder, base_filename)
            return export_filepath
        else:
            inkex.errormsg('Base Filename is *invalid*')
            sys.exit()

    # filename_suffix = (str(time.time())).replace('.', '')

    # Inkscape SVG empty shell
    def get_inkscape_svg_shell(self, source_svg):
        from copy import deepcopy

        copied_svg = deepcopy(source_svg)
        drawable_elements = copied_svg.xpath('//svg:circle | //svg:ellipse | //svg:image | //svg:line | //svg:path | '
                                             '//svg:polygon | //svg:polyline | //svg:rect | //svg:svg | //svg:text | '
                                             '//svg:textPath | //svg:title | //svg:tspan | //svg:use | //svg:g')
        for element in drawable_elements:
            element.delete()
        return copied_svg
