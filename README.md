# Ink Tesseract

Ink Tesseract
An Inkscape 1.1+ extension

Ink Tesseract a partially completed Text OCR extension, which uses google tesseract

Tesseract is included with most Linux distributions

Windows Binaries:

https://github.com/UB-Mannheim/tesseract/wiki
